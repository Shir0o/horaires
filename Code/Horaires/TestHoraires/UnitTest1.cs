﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Horaires
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIsOpenOnWednesdaySuccess()
        {
            DB db = new DB();

            DateTime Wednesday = new DateTime(2016, 05, 11, 12, 22, 11);

            bool expectedResult = true;
            bool actualResult = db.IsOpenOn(Wednesday);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIsOpenOnThursdaySuccess()
        {
            DB db = new DB();

            DateTime Thursday = new DateTime(2016, 05, 12, 12, 22, 11);

            bool expectedResult = false;
            bool actualResult = db.IsOpenOn(Thursday);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIsOpenOnSundaySuccess()
        {
            DB db = new DB();

            DateTime Sunday = new DateTime(2016, 05, 15, 09, 15, 00);

            bool expectedResult = false;
            bool actualResult = db.IsOpenOn(Sunday);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void NextOpeningDateThursday_AfternoonSuccess()
        {
            DB db = new DB();

            DateTime Thursday_Afternoon = new DateTime(2016, 05, 12, 14, 00, 00);

            DateTime expectedDate = new DateTime(2016, 05, 13, 08, 00, 00);

            DateTime ActualDate = db.NextOpeningDate(Thursday_Afternoon);

            Assert.AreEqual(expectedDate, ActualDate);
        }

        [TestMethod]
        public void NextOpeningDateSaturdaySuccess()
        {
            DB db = new DB();

            DateTime Saturday = new DateTime(2016, 05, 14, 09, 15, 00);

            DateTime expectedDate = new DateTime(2016, 05, 16, 08, 00, 00);

            DateTime ActualDate = db.NextOpeningDate(Saturday);

            Assert.AreEqual(expectedDate, ActualDate);
        }

        [TestMethod]
        public void NextOpeningDateThursdaySuccess()
        {
            DB db = new DB();

            DateTime Thursday = new DateTime(2016, 05, 12, 12, 22, 11);

            DateTime expectedDate = new DateTime(2016, 05, 12, 14, 00, 00);

            DateTime ActualDate = db.NextOpeningDate(Thursday);

            Assert.AreEqual(expectedDate, ActualDate);
        }
    }
}
