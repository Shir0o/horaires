﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

namespace Horaires
{
    public class DB
    {
        //Attributs privés
        private SQLiteConnection _dbConnection;
        private SQLiteDataReader _reader;
        private string _dbDirLocation;

        //Constructor
        public DB()
        {
            //Initiation de la connexion à la DB
            _dbDirLocation = AppDomain.CurrentDomain.BaseDirectory + @"\DB\";
            string dbLocation = _dbDirLocation + "Horaires.db";
            _dbConnection = new SQLiteConnection("Data Source=" + dbLocation + ";Version=3;");

            //Si la DB n'existe pas, on la crée
            if (!Directory.Exists(_dbDirLocation)) Directory.CreateDirectory(_dbDirLocation);
            if (!File.Exists(dbLocation))
            {
                SQLiteConnection.CreateFile(dbLocation);
                CreateTable(@"CREATE TABLE 'Horaires' (

                    'ID'    INTEGER NOT NULL,
                    'Day'   TEXT NOT NULL,
                    'OpeningHour'   INTEGER,
                    'OpeningMinute' INTEGER,
                    'ClosingHour'   INTEGER,
                    'ClosingMinute' INTEGER,
                    PRIMARY KEY('ID')
                );

                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(1, 'Mon', 08, 00, 16, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(2, 'Tue', 08, 00, 12, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(3, 'Tue', 14, 00, 18, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(4, 'Wed', 08, 00, 16, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(5, 'Thu', 08, 00, 12, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(6, 'Thu', 14, 00, 18, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(7, 'Fri', 08, 00, 16, 00);
                INSERT INTO 'Horaires'(ID, Day, OpeningHour, OpeningMinute, ClosingHour, ClosingMinute) VALUES(8, 'Mon', 08, 00, 12, 00);");
            }
        }

        //Cette fonction exécute la commande envoyée en paramètre
        private void CreateTable(string query)
        {
            _dbConnection.Open();
            SQLiteCommand command = new SQLiteCommand(query, _dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();
            _dbConnection.Close();
        }

        public bool IsOpenOn(DateTime date)
        {
            //Le résultat est faux par défaut
            bool result = false;

            //Stoque le jour de la semain en ne prenant que les trois premières lettres
            string dayOfWeek = date.DayOfWeek.ToString();
            dayOfWeek = dayOfWeek.Substring(0, 3);

            //Stoque l'heure et les minutes en un nombre (Exemple: 12H00 devient 1200)
            int time = date.Hour * 100 + date.Minute;

            //Récupère les entrées dans la DB dont le jour de la semaine correspond à la date envoyée en paramètre
            SQLiteCommand command = new SQLiteCommand("SELECT OpeningHour, OpeningMinute, ClosingHour, ClosingMInute FROM Horaires WHERE Day == '" + dayOfWeek + "'", _dbConnection);
            _dbConnection.Open();
            _reader = command.ExecuteReader();


            //Pour chaque entrée dans la DB correspondant à la date
            while(_reader.Read())
            {
                //Stoque l'heure et les minutes en un nombre (Exemple: 12H00 devient 1200)
                int openingTime = Int32.Parse(_reader["OpeningHour"].ToString()) * 100 + Int32.Parse(_reader["OpeningMinute"].ToString());
                int closingTime = Int32.Parse(_reader["ClosingHour"].ToString()) * 100 + Int32.Parse(_reader["ClosingMinute"].ToString());

                //Le magasin est ouvert dans ce cas
                if (time >= openingTime && time <= closingTime) result = true;
            }
            _dbConnection.Close();

            return result;
        }

        public DateTime NextOpeningDate(DateTime date)
        {
            //Date à retourner à la fin
            DateTime nextOpeningDate = date;

            bool isDone = false;

            //Tant que la date n'est pas trouvée
            while(isDone == false)
            {
                //Stoque le jour de la semain en ne prenant que les trois premières lettres
                string dayOfWeek = date.DayOfWeek.ToString();
                dayOfWeek = dayOfWeek.Substring(0, 3);

                //Stoque l'heure et les minutes en un nombre (Exemple: 12H00 devient 1200)
                int time = date.Hour * 100 + date.Minute;

                //Récupère les entrées dans la DB dont le jour de la semaine correspond à la date envoyée en paramètre
                SQLiteCommand command = new SQLiteCommand("SELECT OpeningHour, OpeningMinute, ClosingHour, ClosingMInute FROM Horaires WHERE Day == '" + dayOfWeek + "'", _dbConnection);
                _dbConnection.Open();
                _reader = command.ExecuteReader();

                //Pour chaque entrée dans la DB correspondant à la date
                while (_reader.Read())
                {
                    //Stoque l'heure et les minutes en un nombre (Exemple: 12H00 devient 1200)
                    int openingTime = Int32.Parse(_reader["OpeningHour"].ToString()) * 100 + Int32.Parse(_reader["OpeningMinute"].ToString());
                    int closingTime = Int32.Parse(_reader["ClosingHour"].ToString()) * 100 + Int32.Parse(_reader["ClosingMinute"].ToString());

                    //Si le magasin ouvre plus tard que l'heure actuel et que la vérification n'a pas encore été faite
                    if (time < openingTime && isDone == false)
                    {
                        nextOpeningDate = new DateTime(date.Year, date.Month, date.Day, Int32.Parse(_reader["OpeningHour"].ToString()), Int32.Parse(_reader["OpeningMinute"].ToString()), 00);
                        isDone = true;
                    }
                }
                _dbConnection.Close();

                if (isDone == false)
                {
                    //On remet l'heure à zéro, et on passe au jour suivant avant de répéter la boucle pour trouver la prochaine date d'ouverture
                    date = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00);
                    date = date.AddDays(1);
                }
            }

            return nextOpeningDate;
        }
    }
}
